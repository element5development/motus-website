<?php
/****************************************************************************
  THE TEMPLATE FOR DISPLAYING 404 PAGES (BROKEN LINKS)
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <section class="title-container title-container--full block block--full" style="background-image: url('https://motusheals.com/wp-content/uploads/2017/06/fullscreen-background.jpg');">
    <div class="block block--flex">
      <div class="block block--max page--error">   
        <p class="pre-heading">Sorry, the page you requested could not be found.</p>       
        <p class="main-heading">404</p>
        <p class="post-heading">You can either try using our main navigation above or try a key word search using the form below</p>
        <div class="search-container">
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
    <div class="overlay overlay--gradient"></div>
  </section>

</div>
<?php get_footer(); ?>
