<?php 
/****************************************************************************
  MAIN BLOG PAGE 
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <div class="blog-cats block block--max block--flex">
    <nav class="nav--secondary block block--full">
      <ul>
        <li><a href="/blog/">All</a></li>
        <?php wp_list_categories( 
          array(
            'orderby'            => 'id',
            'show_count'         => false,
            'use_desc_for_title' => false,
            'child_of'           => 2,
            'title_li'           => '',
          ) 
        ); ?>
      </ul>
    </nav>
  </div>

  <section class="feed feed--archive block block--max">
    <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="4" scroll="false" button_label="Load More" button_loading_label="Loading..."]'); ?>
  </section>

</div>

<?php get_footer(); ?>