<?php /*
THE FOOTER TEMPLATE FOR OUR THEME
*/ ?>
      
  <?php get_template_part( 'template-parts/content', 'page-footer' ); ?>

  <?php wp_footer(); ?>

</body>
</html>