<?php 
/****************************************************************************
  PAGE INTRO | SPLIT OR FULL
****************************************************************************/
?>

<?php if ( is_page(441) ) { ?>
  <div class="intro-container block block--max block--flex">
    <div class="reasons-title intro-title-container block block--half-full block--dark">
      <h1>
        <?php if ( get_field('intro_title') ) {
          the_field('intro_title'); 
        } else {
          the_title();
        } ?>
      </h1>
    </div>
    <div class="reasons-intro intro-contents-container block block--half-full" >
      <div class="contents block block--flex block--dark">
        <?php if( have_rows('reasons') ): $i = 0;
          while ( have_rows('reasons') ) : the_row(); $i++; ?>
            <div class="single-reason block block--full">
              <div class="counter">
                <?php $num_padded = sprintf("%02d", $i); echo $num_padded; ?> 
              </div>
              <h2><?php the_sub_field('reason_title'); ?></h2>
              <p><?php the_sub_field('reason_contents'); ?></p>
            </div>
          <?php endwhile;
        endif; ?>
      </div>
      <div class="overlay overlay--gradient meassure"></div>
      <div class="overlay background" style="background-image: url('https://motusheals.com/wp-content/uploads/2017/06/intro-background.jpg');"></div>
    </div>
  </div>
<?php } elseif ( is_search() ) { ?>
  <div class="intro-container block block--max block--flex">
    <div class="intro-title-container block block--half-full block--dark">
      <?php $term = (isset($_GET['s'])) ? $_GET['s'] : ''; // Get 's' querystring param ?>
      <h1>Search results for "<?php echo $term ?>"</h1>
    </div>
    <div class="intro-contents-container block block--half-full" style="background-image: url('https://motusheals.com/wp-content/uploads/2017/06/intro-background.jpg');">
      <div class="contents block block--flex block--dark">
        <p>Didn't find what you were looking for? <a href="https://motusheals.com/contact-us/">Send us a message</a> or call us at <a href="tel:<?php the_field('primary_phone', 'options'); ?>">+1 (833) GO-MOTUS</a></p>
      </div>
      <div class="overlay overlay--gradient"></div>
    </div>
  </div>
<?php } elseif ( get_field('intro_style') == 'split' ) { ?>
  <div class="intro-container block block--max block--flex">
    <div class="intro-title-container block block--half-full block--dark">
      <h1>
        <?php if ( get_field('intro_title') ) {
          the_field('intro_title'); 
        } else {
          the_title();
        } ?>
      </h1>
      <?php if ( get_post_type( get_the_ID() ) == 'post' ) { ?>
        <div class="date label"><?php the_date(); ?></div>
        <div class="categories">
          <?php 
            $categories = get_the_category();
            $separator = ' ';
            $output = '';
            if ( ! empty( $categories ) ) {
              foreach( $categories as $category ) {
                  $output .= '<a class="label" href="'.get_category_link( $category->term_id ).'">' . esc_html( $category->name ) . '</a>' . $separator;
              }
              echo trim( $output, $separator );
            }
          ?>
        </div>
      <?php } ?>
    </div>
    <div class="intro-contents-container block block--half-full" style="background-image: url('https://motusheals.com/wp-content/uploads/2017/06/intro-background.jpg');">
      <div class="contents block block--flex block--dark">
        <p><?php the_field('intro_description'); ?></p>
      </div>
      <div class="overlay overlay--gradient"></div>
    </div>
  </div>
<?php } else { ?>
  <div class="intro-container single block block--max block--flex">
    <div class="intro-title-container block block--full block--dark">
      <h1>
        <?php if ( get_field('intro_title') ) {
          the_field('intro_title'); 
        } else {
          the_title();
        } ?>
      </h1>
      <?php if ( get_post_type( get_the_ID() ) == 'post' ) { ?>
        <div class="date label"><?php the_date(); ?></div>
        <div class="categories">
          <?php 
            $categories = get_the_category();
            $separator = ' ';
            $output = '';
            if ( ! empty( $categories ) ) {
              foreach( $categories as $category ) {
                  $output .= '<a class="label" href="'.get_category_link( $category->term_id ).'">' . esc_html( $category->name ) . '</a>' . $separator;
              }
              echo trim( $output, $separator );
            }
          ?>
        </div>
      <?php } ?>
      <?php the_field('intro_contents'); ?>
    </div>
  </div>
<?php } ?>