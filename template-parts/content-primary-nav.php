<?php 
/****************************************************************************
  MAIN NAVIGATION
****************************************************************************/
?>

<?php if (  is_page_template( 'template-styleguide.php' ) ) { ?>
  <header id="sticky-nav" class="primary-nav block block--dark">
    <div class="block block--max block--flex" style="justify-content: center;">
      <a href="/"><img id="nav-logo" src="https://motusheals.com/wp-content/uploads/2016/10/motus-logo-inverse-RGB.svg" alt="Motus Rehabilitation" /></a>
    </div>
  </header>
<?php } elseif (  is_page_template( 'template-construction.php' ) ) { ?>
  <div class="utility-primary">
    <div class="header-container block block--full">
      <header class="landing-nav primary-nav block block--flex block--dark">
          <img id="nav-logo" src="https://motusheals.com/wp-content/uploads/2016/10/motus-logo-inverse-RGB.svg" alt="Motus Rehabilitation" />
      </header>
    </div>
  </div>
<?php } else { ?>
  <div class="utility-primary">
    <div class="utility-nav block block--flex block--full">
      <a href="/new-patients/" class="btn btn--secondary">New patients</a>
			<div>
				<a href="/locations/" class="btn btn--secondary">Locations</a>
				<a href="tel:<?php the_field('primary_phone', 'options'); ?>" class="btn btn--secondary">1-833-GO-MOTUS</a>
			</div>
    </div>
    <div id="sticky-nav" class="header-container block block--full">
      <header class="primary-nav block block--flex block--dark">
          <nav class="nav--primary">
            <?php wp_nav_menu( array( 'theme_location' => 'primary-nav-left' ) ); ?>
          </nav>
          <a href="/">
            <img id="nav-logo" src="https://motusheals.com/wp-content/uploads/2016/10/motus-logo-inverse-RGB.svg" alt="Motus Rehabilitation" />
            <img id="nav-logo-sm" src="https://motusheals.com/wp-content/uploads/2017/06/Motus-logo-inverse-sm.svg" alt="Motus Rehabilitation" />
          </a>
          <nav class="nav--primary">
            <?php wp_nav_menu( array( 'theme_location' => 'primary-nav-right' ) ); ?>
          </nav> 
      </header>
      <div class="search-container block block--full">
        <div class="block block--max"> 
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
  </div>

  <div class="utility-primary mobile">
    <div class="utility-nav block block--flex block--full">
      <a href="/new-patients/" class="btn btn--secondary">New patients</a>
      <div id="mobile-search" class="btn btn--secondary">
        <svg id="search-cta" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" enable-background="new 0 0 50 50">
          <path fill="#000000" d="M20.1 20.6c.2 5.5 4.9 9.7 10.4 9.5 5.1-.2 9.3-4.3 9.5-9.5-.2-5.5-4.9-9.7-10.4-9.5-5.1.2-9.2 4.3-9.5 9.5zm-8.1 0c.3-10 8.7-17.8 18.7-17.5 9.5.3 17.2 8 17.5 17.5 0 10-8.1 18.1-18.1 18.1-10-.1-18.1-8.1-18.1-18.1zm-6.8 26.4c-1.8 0-3.2-1.5-3.2-3.2 0-.9.4-1.7 1-2.3l9-8.1c1.2-1.2 3.2-1.3 4.5 0s1.3 3.2 0 4.5c-.1.1-.2.2-.3.2l-9 8.1c-.5.5-1.3.8-2 .8m8.2-9.5c-.4 0-.7-.2-1-.4-.5-.6-.4-1.4.1-1.9l4.7-4.3c.6-.5 1.4-.4 1.9.1s.5 1.4-.1 1.9l-4.7 4.3c-.3.1-.6.3-.9.3"></path>
        </svg>
        search
      </div>
    </div>
    <div id="sticky-mobile-nav" class="header-container block block--full">
      <header class="primary-nav block block--flex block--dark">
          <a href="tel:<?php the_field('primary_phone', 'options'); ?>">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" enable-background="new 0 0 50 50">
              <path d="M9.7 2.7c-.3-.1-.7-.1-.9.1-3.3 1.3-5.4 6.1-5.9 9.3-1.6 10.6 6.8 19.8 14.5 25.9 6.9 5.4 20 14.2 27.8 5.7 1-1.1 2.2-2.6 2.1-4.1-.2-2.6-2.6-4.4-4.4-5.9-1.4-1.1-4.4-4-6.2-3.9-1.7.1-2.7 1.8-3.8 2.8l-1.9 1.9c-.3.3-4.3-2.3-4.7-2.6-1.6-1.1-3.2-2.2-4.5-3.5-1.4-1.3-2.6-2.7-3.6-4.2-.3-.4-2.8-4.2-2.6-4.5 0 0 2.2-2.3 2.8-3.3 1.3-2 2.3-3.5.8-5.9-.6-.9-1.2-1.5-2-2.3-1.3-1.2-2.6-2.5-4.1-3.6-.8-.6-2.2-1.7-3.4-1.9z"></path>
            </svg>
          </a>
          <a href="/">
            <img id="nav-logo" src="https://motusheals.com/wp-content/uploads/2016/10/motus-logo-inverse-RGB.svg" alt="Motus Rehabilitation" />
            <img id="nav-logo-sm" src="https://motusheals.com/wp-content/uploads/2017/06/Motus-logo-inverse-sm.svg" alt="Motus Rehabilitation" />
          </a>
          <button class="hamburger hamburger--collapse" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
      </header>
      <div class="mobile-nav-container block block--full">
        <nav class="nav--primary block block--flex">
          <div class="block block--full">
						<?php wp_nav_menu( array( 'theme_location' => 'primary-nav-left' ) ); ?>
						<div class="menu-primary-navigation-location-container">
							<ul id="menu-primary-navigation-location-1" class="menu">
								<li class="menu-item">
									<a href="/locations/">Locations</a>
								</li>
							</ul>
						</div>
            <?php wp_nav_menu( array( 'theme_location' => 'primary-nav-right' ) ); ?>
          </div>
        </nav>
      </div>
      <div class="search-container block block--full">
        <div class="block block--max"> 
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
  </div>
<?php } ?>