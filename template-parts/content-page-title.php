<?php 
/****************************************************************************
  PAGE TITLE BANNER
****************************************************************************/
?>

<?php //GET FEATURED IMAGE
if ( has_post_thumbnail() && get_post_type( get_the_ID() ) == 'post' ) {
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  $thumb_url = $thumb_url_array[0];
} else if ( is_home() || is_archive() || get_post_type( get_the_ID() ) == 'post' ) {
  $thumb_url_array = get_field('default_post_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else {
  $thumb_url_array = get_field('default_page_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} ?> 

<?php if ( is_page(4) ) { ?>
  <?php $background = get_field('background_image'); ?>
  <section class="title-container title-container--full block block--full">
    <div class="block block--flex">
      <div class="block block--max">
        <?php if ( get_field('main_heading') ) { ?>
          <p class="pre-heading"><?php the_field('pre-heading'); ?></p>
          <p class="main-heading"><?php the_field('main_heading'); ?></p>
          <p class="post-heading"><?php the_field('post-heading'); ?></p>
        <?php } else { ?>
          <p class="main-heading"><?php the_title(); ?></p>
        <?php } ?>
        <?php if ( get_field('cta_url') ) { ?>
          <a href="<?php the_field('cta_url'); ?>" class="btn btn--primary"><?php the_field('cta_text'); ?></a>
        <?php } ?>
        <?php if (  is_page_template( 'template-construction.php' ) ) { echo do_shortcode('[gravityform id="4" title="false" description="false"]'); } ?>
      </div>
    </div>
    <div class="overlay overlay--gradient"></div>
    <div class="overlay overlay--image" style="background-image: url('<?php echo $background['url']; ?>');"></div>
  </section>
<?php } else if ( is_search() ) { ?>
  <section class="title-container block block--full block--flex" style="background-image: url('<?php echo $thumb_url; ?>');">
    <div class="block block--max">
      <p class="main-heading">Search Results</p>
    </div>
  </section>
<?php } else if ( is_home() || is_archive() ) { ?>
   <!-- GET FEATURED POST-->
   <?php 
      $args = array( 
        'posts_per_page'  => 1, 
        'post_type' => 'post',
        'meta_key'    => 'featured_post',
        'meta_value'  => 'yes'
      );
      $query = new WP_Query( $args );
    ?>
    <?php if ( $query->have_posts() ) { ?> 
      <?php while ( $query->have_posts() ) { $query->the_post(); ?>
        <!--GET IMAGE-->
        <?php //GET FEATURED IMAGE
          if ( has_post_thumbnail() && get_post_type( get_the_ID() ) == 'post' ) {
            $thumb_id = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
            $thumb_url = $thumb_url_array[0];
          } else {
            $thumb_url_array = get_field('default_post_image', 'options'); 
            $thumb_url = $thumb_url_array['url'];
          }
        ?>
        <section class="blog-title title-container block block--full block--flex" style="background-image: url('<?php echo $thumb_url; ?>');">
          <div class="block block--max">
            <p class="featured-text">featured post</p>
            <p class="main-heading"><?php the_title(); ?></p>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="btn btn--primary">read the rest</a>
          </div>
          <div class="overlay overlay--gradient"></div>
        </section>
      <?php } ?>
    <?php } ?>
<?php } else if ( get_post_type( get_the_ID() ) == 'post' ) { ?>
  <section class="title-container block block--full block--flex" style="background-image: url('<?php echo $thumb_url; ?>');"></section>
<?php } else { ?>
  <section class="title-container block block--full block--flex" style="background-image: url('<?php echo $thumb_url; ?>');">
    <div class="block block--max">
      <?php if ( get_field('main_heading') ) { ?>
        <p class="pre-heading"><?php the_field('pre-heading'); ?></p>
        <p class="main-heading"><?php the_field('main_heading'); ?></p>
        <p class="post-heading"><?php the_field('post-heading'); ?></p>
				<?php if ( get_field('button') ) { ?>
					<?php $link = get_field('button'); ?>
					<a href="<?php echo $link['url']; ?>" class="btn btn--secondary" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
        <?php } ?>
      <?php } elseif ( is_singular('staff') ) { ?>
        <p class="main-heading">Meet our team</p>
        <p class="post-heading">of Occupational Therapists, Physical Therapists, and Orthotists.</p>
      <?php } else { ?>
        <p class="main-heading"><?php the_title(); ?></p>
      <?php } ?>
    </div>
  </section>
<?php } ?>