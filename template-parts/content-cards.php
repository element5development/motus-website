<?php 
/****************************************************************************
  LINK/DOWNLOAD CARDS
****************************************************************************/
?>

<?php if( have_rows('cards') ): $total_cards = 0;
  while ( have_rows('cards') ) : the_row(); 
    $total_cards++;
  endwhile;
endif; ?>

<div class="cards-container block block--flex block--max">

  <?php if ( get_field('section_title') ) { ?>
    <h2><?php the_field('section_title'); ?></h2>
  <?php } ?>

  <?php if ( get_field('card_style') == 'download' ) { ?>

      <?php if( have_rows('cards') ):
        while ( have_rows('cards') ) : the_row(); ?>

              <a target="_blank" href="<?php the_sub_field('cta_url'); ?>" class="card card--basic block block--dark block--third">
                <img src="https://motusheals.com/wp-content/uploads/2016/10/icon-download.svg" alt="file download" />
                <h2><?php the_sub_field('title'); ?></h2>
                <div class="btn btn--primary"><?php the_sub_field('cta_text'); ?></div>
              </a>

        <?php endwhile;
      endif; ?>

  <?php } else { ?>

      <?php if( have_rows('cards') ): $i == 0;
        while ( have_rows('cards') ) : the_row(); $i++; ?>

          <?php if( get_sub_field('cta_url') ) { ?>
            <a <?php if ( the_field('card_style') == 'offsite' ) { ?> target="_blank" <?php } ?> href="<?php the_sub_field('cta_url'); ?>"  class="card card--full block block--dark block--third">
          <?php } else { ?> 
            <div  class="card card--full block block--dark block--third">
          <?php } ?>
            <?php $image = get_sub_field('icon'); ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <h2><?php the_sub_field('title'); ?></h2>
            <?php the_sub_field('description'); ?>
            <?php if( get_sub_field('cta_text') ) { ?><div class="btn btn--primary"><?php the_sub_field('cta_text'); ?></div><?php } ?>
            <?php if ( is_page_template( 'template-construction.php' ) && $i == 3 ) { echo do_shortcode('[gravityform id="4" title="false" description="false"]'); } ?>
          <?php if( get_sub_field('cta_url') ) { ?></a><?php } else { ?></div><?php } ?>

        <?php endwhile;
      endif; ?>

  <?php } ?>
</div>