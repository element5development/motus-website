<?php 
/****************************************************************************
  CERTIFICATIONS & MEMBERSHIPS SLICK SLIDER
****************************************************************************/
?>

<div class="logo-slider-container block block--full">
  <?php if ( is_singular('staff') ) { ?>
    <img src="https://motusheals.com/wp-content/uploads/2016/10/Motus-logomark-white.svg" alt="Motus Rehabilitation" />
  <?php } ?>
  <h2><?php if ( is_singular('staff') ) { echo "Motus's"; } ?> certifications and memberships</h2>
  <div class="logo-slider block block--max">
  <?php if( have_rows('certifications_and_memberships', 'options') ):
      while ( have_rows('certifications_and_memberships', 'options') ) : the_row(); $image = get_sub_field('logo'); ?>
        <div class="single-logo"><a target="_blank" href="<?php the_sub_field('url'); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a></div>
      <?php endwhile;
    endif; ?>
  </div>
</div>