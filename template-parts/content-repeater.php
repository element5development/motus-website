<?php 
/****************************************************************************
  REPEATER TEMPLATE FOR AJAX LOAD MORE PLUGIN
****************************************************************************/
?>

<?php //GET FEATURED IMAGE
if ( has_post_thumbnail() ) {
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
  $thumb_url = $thumb_url_array[0];
} else {
  $thumb_url_array = get_field('default_page_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} ?> 

<div class="<?php if ( get_post_type( get_the_ID() ) == 'staff' ) { ?>preview-staff <?php } ?> preview-post block bock--max block--flex">
  <a href="<?php the_permalink(); ?>" class="image block block--half-full" style="background-image: url('<?php echo $thumb_url; ?>')"></a>
  <div class="contents block block--half-full block--dark">
    <?php if ( get_post_type( get_the_ID() ) == 'post' ) { ?>
      <div class="meta block block--full block--flex">
        <div class="date label"><?php the_date(); ?></div>
        <div class="categories">
          <?php 
            $categories = get_the_category();
            $separator = ' ';
            $output = '';
            if ( ! empty( $categories ) ) {
              foreach( $categories as $category ) {
                  $output .= '<a class="label" href="'.get_category_link( $category->term_id ).'">' . esc_html( $category->name ) . '</a>' . $separator;
              }
              echo trim( $output, $separator );
            }
          ?>
        </div>
      </div>
    <?php } ?>
    <a href="<?php the_permalink(); ?>">
      <h1><?php the_title(); ?></h1>
      <?php the_excerpt(); ?>
      <div class="btn btn--primary">Read the rest</div>
    </a>
  </div>
</div>