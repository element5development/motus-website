<?php 
/****************************************************************************
  CLOSING PAGE WITH A QUESTION
****************************************************************************/
?>

<?php $background = get_field('question_background'); ?>
<div class="closing-container block block--full" style="background-image: url('<?php echo $background['url']; ?>');">
  <div class="block block--flex block--max">
    <div class="closing-question block block--dark">
      <img src="https://motusheals.com/wp-content/uploads/2016/10/question-mark.svg" alt="file download" />
      <h2>Did you know?</h2>
      <?php the_field('question_contents'); ?>
    </div>
  </div>
</div>