<?php 
/****************************************************************************
  FOOTER CONTENT
****************************************************************************/
?>

<?php if (  is_page_template( 'template-styleguide.php' ) || is_page_template( 'template-construction.php' ) ) { ?>
  <footer id="footer" class="block">
    <div class="copyright block block--dark">
      <div class="block block--max block--flex">
        <p class="p--legal">©Copyright <?php echo date('Y'); ?> Motus Rehabilitation. All Rights Reserved. </p> 
        <p class="p--legal">Crafted by <a href="https://element5digital.com/">Element5</a></p>
      </div>
    </div>
  </footer>
<?php } else { ?>
  <footer id="footer" class="block block--dark">
    <div class="block block--max block--flex">
      <img src="https://motusheals.com/wp-content/uploads/2016/10/Motus-Logo-inverse.svg" alt="Motus Rehabilitation" />
      <nav>
				<?php wp_nav_menu( array( 'theme_location' => 'footer-nav' ) ); ?>
				<?php wp_nav_menu( array( 'theme_location' => 'location-nav' ) ); ?>
			</nav>
    </div>
    <div class="copyright block block--dark">
      <div class="block block--max">
        <p class="p--legal">©Copyright <?php echo date('Y'); ?> Motus Rehabilitation. All Rights Reserved. </p> 
        <p id="element5-credit" class="p--legal">Crafted by <a href="https://element5digital.com/">Element5</a></p>
      </div>
    </div>
  </footer>
<?php } ?>