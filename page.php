<?php 
/****************************************************************************
  DEFAULT PAGE TEMPLATE
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <?php if ( get_field('intro_title') ) { 
    get_template_part( 'template-parts/content', 'page-intro' ); 
  } ?>

  <?php if ( $post->post_content != "" ) { ?>
    <?php if( is_page(449) ) { ?>
      <div class="block block--max block--flex default-contents">
        <h2><b>We’re here to help </b></h2>
        <div class="block block--half">
          <?php the_content(); ?>
        </div>
        <div class="block block--half">
          <?php if( have_rows('locations_repeater', 'options') ):
            while ( have_rows('locations_repeater', 'options') ) : the_row(); ?>
              <div class="single-location-preview block block--full">
                <h3><?php the_sub_field('city', 'options'); ?></h3>
                <p><a target="_blank" href="https://maps.google.com/?q=<?php the_sub_field('address', 'options'); ?>, <?php the_sub_field('city', 'options'); ?>, <?php the_sub_field('state', 'options'); ?> <?php the_sub_field('zip', 'options'); ?>">
                  <?php the_sub_field('address', 'options'); ?>, <?php the_sub_field('city', 'options'); ?>, <?php the_sub_field('state', 'options'); ?> <?php the_sub_field('zip', 'options'); ?>
                </a></p>
                <p>
                  phone: <a href="tel:<?php the_sub_field('phones', 'options'); ?>"><?php the_sub_field('phone', 'options'); ?></a>
                  <?php if ( get_sub_field('fax', 'options') ) { ?>
                    | <span>fax: <?php the_sub_field('fax', 'options'); ?></span>
									<?php } ?>
								</p>
								<?php 
									$slug = str_replace(' ', '-',strtolower(get_sub_field('city', 'options'))); 
								?>
								<!-- <a href="/location/<?php echo $slug; ?>" class="btn btn--secondary">Visit this Location</a> -->
              </div>
            <?php endwhile;
          endif; ?>
        </div>
      </div>
    <?php } else { ?>
      <div class="block block--max default-contents">
    		<?php the_content(); ?>
      </div>
    <?php } ?>
  <?php } ?>

  <?php if ( have_rows('cards') ) {
    get_template_part( 'template-parts/content', 'cards' ); 
  } ?>

  <?php if ( get_field('display_logo_slider') == 'yes' ) {
    get_template_part( 'template-parts/content', 'logo-slider' ); 
  } ?>

  <?php if ( get_field('display_question') == 'yes' ) {
    get_template_part( 'template-parts/content', 'closing-question' ); 
  } ?>
  
</div>

<?php get_footer(); ?>