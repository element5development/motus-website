<?php 
/****************************************************************************
  Template Name: Home
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <div class="home-intro intro-container block block--max block--flex">
    <div class="intro-title-container block block--half-full block--dark">
      <svg xmlns="https://www.w3.org/2000/svg" width="55" height="53" viewBox="0 0 55 53"><path fill="#00b59a" d="M22.44 20.98c0 6.36 5.18 11.54 11.54 11.54 6.36 0 11.54-5.18 11.54-11.54 0-6.36-5.18-11.54-11.54-11.54a11.56 11.56 0 0 0-11.54 11.54zm-9.44 0a21.01 21.01 0 0 1 20.98-20.98 21.01 21.01 0 0 1 20.98 20.98 21.01 21.01 0 0 1-20.98 20.98 21.01 21.01 0 0 1-20.98-20.98zm-8.32 31.8a3.6 3.6 0 0 1-2.74-1.22 3.65 3.65 0 0 1 .28-5.18l10.48-9.44a3.67 3.67 0 1 1 4.92 5.46l-10.5 9.44a3.6 3.6 0 0 1-2.44.94m9.9-12.7a1.58 1.58 0 0 1-1.06-2.74l5.5-4.94a1.58 1.58 0 0 1 2.22.12c.6.66.54 1.64-.12 2.22l-5.5 4.94c-.3.26-.68.4-1.04.4"></path></svg>
      <h1><?php the_field('intro_title'); ?></h1>
      <p><?php the_field('therapist_search'); ?></p>
      <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
      <p>Or call us: <a href="tel:<?php the_field('primary_phone','options') ?>">1-833-GO-MOTUS</a></p>
    </div>
    <div class="intro-contents-container block block--half-full" style="background-image: url('https://motusheals.com/wp-content/uploads/2017/06/intro-background.jpg');">
      <div class="contents block block--flex block--dark">
        <p><?php the_field('intro_contents'); ?></p>
      </div>
      <div class="overlay overlay--gradient"></div>
    </div>
    <div class="scroll-noteification"></div>
  </div>

  <?php if ( $post->post_content != "" ) { ?>
    <div class="block block--max default-contents">
      <?php the_content(); ?>
    </div>
  <?php } ?>

  <div class="fixed-content-parent">
    <div class="block block--max">
      <div class="rehab-stats block block--dark">
        <img src="https://motusheals.com/wp-content/uploads/2016/05/motus-logomark-one-color.png" alt="Motus Rehabilitation" />
        <h3>Motus Rehabilitation <br/>at a glance</h3>
        <?php if( have_rows('stats') ): $i==0;
          while ( have_rows('stats') ) : the_row(); ?>
            <div class="stat block">
              <p class="stat-num">
                <span class="counter" data-count="<?php the_sub_field('number_one'); ?>">0</span>
                <?php if ( get_sub_field('number_two') ) { ?>
                -
                <span class="counter" data-count="<?php the_sub_field('number_two'); ?>">0</span>
                <?php } ?>
              </p>
              <p><?php the_sub_field('description'); ?></p>
            </div>
            <hr>
          <?php endwhile;
        endif; ?>
        <a href="<?php the_field('learn_more_url'); ?>" class="btn btn--secondary">Learn More</a>
      </div>
    </div>

    <?php if( have_rows('cta_sections') ): $i==0;
      while ( have_rows('cta_sections') ) : the_row(); $i++; $icon = get_sub_field('icon'); $background = get_sub_field('background'); ?>
        <div class="home-cta-container <?php if( $i == 2 ) { ?>z-drop<?php } ?> closing-container block block--full">
          <div class="block block--flex block--max">
            <a href="<?php the_sub_field('url'); ?>" class="<?php if( $i == 2 ) { ?>fade-animate<?php } ?> closing-question block <?php if( $i != 2 ) { ?>block--dark<?php } ?>">
              <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
              <h2><?php the_sub_field('title'); ?></h2>
              <p><?php the_sub_field('contents'); ?></p>
              <?php if( $i == 2 ) { ?>
                <div class="btn tn--primary" href="<?php the_sub_field('url'); ?>">Learn More</div>
              <?php } ?>
            </a>
          </div>
          <?php if( $i != 2 ) { ?>
            <div class="block block--max">
              <a class="closing-cta" href="<?php the_sub_field('url'); ?>">Learn More<svg xmlns="https://www.w3.org/2000/svg" width="12" height="13" viewBox="0 0 12 13"><path fill="#c5e300" d="M10.82 6.81l-9.65 5.61a.77.77 0 0 1-1.06-.28.77.77 0 0 1-.11-.39v-10.97c0-.43.35-.78.78-.78.13 0 .26.03.37.1l9.65 5.36c.38.21.51.69.31 1.06a.8.8 0 0 1-.29.29z"></path></svg></a>
            </div>
          <?php } ?>
          <?php if( $i != 2 ) { ?>
            <div class="overlay overlay--image" style="background-image: url('<?php echo $background['url']; ?>');"></div>
          <?php } ?>
        </div>
      <?php endwhile;
    endif; ?>
  </div>

  <?php if ( get_field('display_social') == 'yes' ) {
    get_template_part( 'template-parts/content', 'follow-social' ); 
  } ?>

  <?php if ( get_field('display_logo_slider') == 'yes' ) {
    get_template_part( 'template-parts/content', 'logo-slider' ); 
  } ?>

  <?php if ( get_field('display_question') == 'yes' ) {
    get_template_part( 'template-parts/content', 'closing-question' ); 
  } ?>
  
</div>

<?php get_footer(); ?> 


