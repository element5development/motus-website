/* DECLARATIONS FOR ANIMATIONS, STICKY PORTIONS, SLIDERS, ETC. | ONLY MINIFIED VERSIONS ARE CALLED IN THEME */


/****************************************************************************
    HAMBURGER MENU
****************************************************************************/
jQuery(document).ready(function () {
	jQuery('.hamburger').click(function () {
		jQuery('.mobile-nav-container').toggleClass('open');
		jQuery(this).toggleClass('is-active')
	});
});
/****************************************************************************
    SEARCH FORM
****************************************************************************/
jQuery(document).ready(function () {
	jQuery('#menu-item-399').click(function () {
		jQuery('.search-container').toggleClass('active');
		jQuery(this).toggleClass('active');
	});
	jQuery('#mobile-search').click(function () {
		jQuery('.search-container').toggleClass('active');
		jQuery(this).toggleClass('active');
	});
});
/****************************************************************************
    COUNT UP
****************************************************************************/
jQuery(document).ready(function () {
	jQuery(".counter").viewportChecker({
		classToAdd: "count",
		offset: 100
	});
});
jQuery(document).ready(function () {
	jQuery(window).scroll(function () {
		jQuery('.count').each(function () {
			var thisdiv = jQuery(this),
				countTo = thisdiv.attr('data-count');
			jQuery({
				countNum: thisdiv.text()
			}).animate({
				countNum: countTo
			}, {
				duration: 1000,
				easing: 'linear',
				step: function () {
					thisdiv.text(Math.floor(this.countNum));
				},
				complete: function () {
					thisdiv.text(this.countNum);
					//alert('finished');
				}
			});
		});
	});
});
/****************************************************************************
    ENTRANCE ANIMATIONS
****************************************************************************/
jQuery(document).ready(function () {
	jQuery(".home-intro.intro-container .scroll-noteification").addClass("hidden").viewportChecker({
		classToAdd: "visible animated slideInUp",
		offset: 0
	});
	jQuery(".fade-animate.closing-question").addClass("hidden").viewportChecker({
		classToAdd: "visible animated fadeIn",
		offset: 325
	});
	jQuery(".closing-question:not(.fade-animate )").addClass("hidden").viewportChecker({
		classToAdd: "visible animated slideInUp",
		offset: 325
	});
	jQuery(".closing-cta").addClass("hidden").viewportChecker({
		classToAdd: "visible animated slideInDown",
		offset: 150
	});
	jQuery(".preview-staff:not(.first)").addClass("hidden").viewportChecker({
		classToAdd: "visible animated fadeIn",
		offset: 200
	});
});
/****************************************************************************
    REASONS ANIMATED CLIPPING
****************************************************************************/
jQuery(window).scroll(function () {
	if (window.location.pathname == "/why-choose-us/") {
		var position = jQuery(".meassure").offset().top;
		position = position - 612;
		var positionBottom = position + 375;
		console.log(position, "bottom of container");
		jQuery(".reasons-intro").css("clip-path", "inset(" + position + "px 0px calc(100% - " + positionBottom + "px) 0px)");
	}
});
/****************************************************************************
    STICKY PORTIONS
****************************************************************************/
jQuery(document).ready(function () {
	var window_width = jQuery(window).width();
	if (window_width < 750) {
		//ENABLE STICKY PARTS
		jQuery("#sticky-mobile-nav").stick_in_parent({
			parent: "body"
		});
		//DISABLE STICKY PARTS ON MOBILE
		jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
		jQuery(".reaasons-title").trigger("sticky_kit:detach");
		jQuery(".reasons-intro .overlay").trigger("sticky_kit:detach");
	} else if (window_width < 800) {
		//ENABLE STICKY PARTS
		jQuery("#sticky-mobile-nav").stick_in_parent({
			parent: "body"
		});
		jQuery(".rehab-stats").stick_in_parent({
				parent: ".fixed-content-parent"
			})
			.on("sticky_kit:bottom", function (e) {
				jQuery(this).addClass('bottom');
				jQuery(".fixed-content-parent").addClass('bottom');
			})
			.on("sticky_kit:unbottom", function (e) {
				jQuery(this).removeClass('bottom');
				jQuery(".fixed-content-parent").removeClass('bottom');
			});
		//DISABLE STICKY PARTS ON MOBILE
		jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
		jQuery(".reaasons-title").trigger("sticky_kit:detach");
		jQuery(".reasons-intro .overlay").trigger("sticky_kit:detach");
	} else {
		//ENABLE STICKY PARTS
		jQuery("#sticky-nav").stick_in_parent({
			parent: "body"
		});
		jQuery("#styleguide-sidebar").stick_in_parent({
			offset_top: 116
		});
		jQuery(".staff-title").stick_in_parent({
			offset_top: 166
		});
		jQuery(".rehab-stats").stick_in_parent({
				parent: ".fixed-content-parent"
			})
			.on("sticky_kit:bottom", function (e) {
				jQuery(this).addClass('bottom');
				jQuery(".fixed-content-parent").addClass('bottom');
			})
			.on("sticky_kit:unbottom", function (e) {
				jQuery(this).removeClass('bottom');
				jQuery(".fixed-content-parent").removeClass('bottom');
			});
		jQuery(".reaasons-title").stick_in_parent({
			offset_top: 166
		});
		jQuery(".reasons-intro .overlay").stick_in_parent({
				offset_top: 230
			})
			.on("sticky_kit:bottom", function (e) {
				jQuery(this).addClass('bottom');
				jQuery(".reasons-intro .overlay.cover").addClass('bottom');
			})
			.on("sticky_kit:unbottom", function (e) {
				jQuery(this).removeClass('bottom');
				jQuery(".reasons-intro .overlay.cover").removeClass('bottom');
			});
	}
	jQuery(window).resize(function () {
		window_width = jQuery(window).width();
		if (window_width < 800) {
			//ENABLE STICKY PARTS
			jQuery("#sticky-mobile-nav").stick_in_parent({
				parent: "body"
			});
			//DISABLE STICKY PARTS ON MOBILE
			jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
			jQuery(".reaasons-title").trigger("sticky_kit:detach");
			jQuery(".reasons-intro .overlay").trigger("sticky_kit:detach");
		} else {
			//ENABLE STICKY PARTS
			jQuery("#sticky-nav").stick_in_parent({
				parent: "body"
			});
			jQuery("#styleguide-sidebar").stick_in_parent({
				offset_top: 116
			});
			jQuery(".staff-title").stick_in_parent({
				offset_top: 166
			});
			jQuery(".reaasons-title").stick_in_parent({
				offset_top: 166
			});
			jQuery(".reasons-intro .overlay").stick_in_parent({
					offset_top: 230
				})
				.on("sticky_kit:bottom", function (e) {
					jQuery(this).addClass('bottom');
					jQuery(".reasons-intro .overlay.cover").addClass('bottom');
				})
				.on("sticky_kit:unbottom", function (e) {
					jQuery(this).removeClass('bottom');
					jQuery(".reasons-intro .overlay.cover").removeClass('bottom');
				});
		}
	});
});
/****************************************************************************
    SLICK SLIDER 
****************************************************************************/
jQuery(document).ready(function () {
	jQuery('.logo-slider').slick({
		arrows: false,
		dots: false,
		autoplay: true,
		autoplaySpeed: 2250,
		infinite: true,
		speed: 300,
		slidesToShow: 5,
		slidesToScroll: 1,
		responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
});

/*------------------------------------------------------------------
  	LOCATION MAPS
	------------------------------------------------------------------*/
var $ = jQuery;

$(document).ready(function () {
	$('.acf-map').each(function () {
		map = new_map($(this));
	});

	function new_map($el) {
		var $markers = $el.find('.marker');
		var args = {
			zoom: 16,
			center: new google.maps.LatLng(0, 0),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map($el[0], args);
		map.markers = [];
		$markers.each(function () {
			add_marker($(this), map);
		});
		center_map(map);
		return map;
	}

	function add_marker($marker, map) {
		var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
		var marker = new google.maps.Marker({
			position: latlng,
			map: map
		});
		map.markers.push(marker);
		if ($marker.html()) {
			var infowindow = new google.maps.InfoWindow({
				content: $marker.html()
			});
			google.maps.event.addListener(marker, 'click', function () {
				infowindow.open(map, marker);
			});
		}
	}

	function center_map(map) {
		var bounds = new google.maps.LatLngBounds();
		$.each(map.markers, function (i, marker) {
			var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
			bounds.extend(latlng);
		});
		if (map.markers.length == 1) {
			map.setCenter(bounds.getCenter());
			map.setZoom(16);
		} else {
			map.fitBounds(bounds);
		}
	}
});