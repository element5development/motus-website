<?php 
/*-------------------------------------------------------------------
    Template Name: Locations
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <?php if ( get_field('intro_title') ) { 
    get_template_part( 'template-parts/content', 'page-intro' ); 
  } ?>

	<?php
		$args = array(
			'post_type'      => 'location',
			'orderby' 			 => 'title',
			'order' 				 => 'ASC',
			'posts_per_page' => -1,
		);
		$locations = new WP_Query( $args );
	?>
	<?php if ( $locations->have_posts() ) : ?>
		<div class="default-contents">
			<div class="acf-map">
			<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
					<?php $location = get_field('map'); ?>
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
						<h4><?php the_field('city'); ?></h4>
						<p class="address"><?php the_field('address'); ?> <?php the_field('address_line_2'); ?></p>
						<?php $mapLink = 'https://maps.google.com/?q=' . get_field('address') . get_field('address_line_2') . get_field('city') . get_field('state') . get_field('zip'); ?>
						<a target="_blank" href="<?php echo $mapLink ?>">view on google maps</a>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
		<?php endif; wp_reset_postdata(); ?>
		<div class="default-contents">
			<?php the_content(); ?>
		</div>
		<?php if ( $locations->have_posts() ) : ?>
		<div class="default-contents">
			<div class="locations-container">
				<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
					<div class="single-location">
						<a href="<?php the_permalink(); ?>"></a>
						<h2><?php the_field('city'); ?></h2>
						<?php $phone = preg_replace( '/[^0-9]/', '', get_field('phone') ); ?>
						<address>
							<?php the_field('address'); ?>
							<?php the_field('address_line_2'); ?><br/>
							<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?><br/>
						</address>						
						<?php if( have_rows('services') ): ?>
						<h3>Services</h3>
						<ul>
							<?php while ( have_rows('services') ) : the_row(); ?>
							<li><?php the_sub_field('service'); ?></li>
							<?php endwhile; ?>
						</ul>
						<?php else :
							// no rows found
						endif; ?>

						<a class="btn" href="tel:+1<?php echo $phone; ?>"><?php the_field('phone'); ?></a>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; wp_reset_postdata(); ?>
</div>
<div class="closing-container block block--full" style="background-image: url('/wp-content/uploads/2017/06/beach.jpg');">
	<div class="block block--flex block--max">
		<div class="closing-question block block--dark">
			<img src="/wp-content/uploads/2019/03/make-appointment-icon-2.svg" alt="location pointer" />
			<h2>Make an Appointment</h2>
			<p>Do you want to be seen by one of our physical therapists? Make an appointment online. </p>
			<a href="/make-an-appointment/" class="btn" target="">Schedule Appointment</a>
		</div>
	</div>
</div>

<?php get_footer(); ?>