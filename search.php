<?php 
/****************************************************************************
  THE CONTAINER FOR SEARCH RESULTS PAGES
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <?php get_template_part( 'template-parts/content', 'page-intro' ); ?>

  <section class="feed feed--search block block--max">
    <?php 
      $term = (isset($_GET['s'])) ? $_GET['s'] : ''; // Get 's' querystring param
      echo do_shortcode('[ajax_load_more id="relevanssi" container_type="div" search="'. $term .'" post_type="post, page, staff" posts_per_page="4" scroll="false" button_label="Load More" button_loading_label="Loading..."]');
    ?>
  </section>

  <?php get_template_part( 'template-parts/content', 'logo-slider' ); ?>

</div>

<?php get_footer(); ?>