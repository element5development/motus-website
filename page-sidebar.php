<?php 
/****************************************************************************
  Template Name: Page with Sidebar
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">
  <div class="block block--max block--flex">
  
    <div class="page--sidebar">
    	<?php if ( !empty(get_the_content()) ) { ?>
        <div class="block block--max default-contents">
    			<?php the_content(); ?>
        </div>
    	<?php } ?>
    </div>
    <?php get_sidebar(); ?>

  </div>
</div>

<?php get_footer(); ?>