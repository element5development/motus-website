<?php
/****************************************************************************
  SINGLE LOCATION PAGE
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

	<div class="intro-container block block--max block--flex">
    <div class="intro-title-container block block--half-full block--dark">
      <?php the_field('google_map_iframe'); ?>
    </div>
    <div class="intro-contents-container block block--half-full" style="background-image: url('https://motusheals.com/wp-content/uploads/2017/06/intro-background.jpg');">
      <div class="contents block block--flex block--dark">
				<div>
					<?php the_field('introduction') ?>
					<p>
						<a href="/make-an-appointment/" class="btn btn--secondary">Make an Appointment</a>
					</p>
					<p>
						<?php the_field('address'); ?><br>
						<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?>
					</p>
					<p>
						phone: <a href="tel:<?php the_field('phones'); ?>"><?php the_field('phone'); ?></a><br/>
						fax: <?php the_field('fax'); ?>
					</p>
				</div>
      </div>
      <div class="overlay overlay--gradient"></div>
    </div>
  </div>

	<div class="block block--max default-contents">
		<?php the_content(); ?>
	</div>

	<?php 
		$location = get_the_title();
		$args = array( 
			'posts_per_page'  => -1, 
			'orderby' => 'date',
			'order' => 'ASC',
			'post_type' => 'staff',
			'category_name' => 'therapists',
			'meta_query'  => array(
				'relation'    => 'OR',
				array(
					'key'   => 'location',
					'value'   => $location,
					'compare' => 'LIKE'
				),
			)
		);
		$query = new WP_Query( $args );
	?>
	<?php if ( $query->have_posts() ) { $i = 0; ?>
  <div class="block block--max block--flex">
 		<div class="location-filter block block--half">
			<h2>The Motus <?php the_title(); ?> Staff</h2>
			<p>Meet the physical therapists at our <?php the_title(); ?> location:</p>
    </div>
    <!-- <nav class="nav--secondary block block--half">
      <ul>
        <li><a href="#therapists" class="smoothScroll">Therapists</a></li>
        <li><a href="#orthotists" class="smoothScroll">Orthotists</a></li>
        <li><a href="#staff" class="smoothScroll">Staff</a></li>
      </ul>
    </nav> -->
  </div>
	<?php } ?>

  <div class="special-staff block block--max default-contents">
		<?php if ( $query->have_posts() ) { $i = 0; ?>
    <div class="block block--flex block--full">
      <a id="therapists" class="anchor"></a>
      <div class="block block--half">
        <div class="staff-title block block--full block--dark">
          <h1>Therapists</h1>
        </div>
      </div>
      <div class="block block--half">
				<?php while ( $query->have_posts() ) { $query->the_post(); $i++; ?>
					<?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium'); ?>
					<a href="<?php the_permalink(); ?>" class="preview-staff block block--full <?php if( $i == 1) { ?>first<?php } ?>">
						<div class="intro block block--dark" style="background-image: url('<?php echo $featured[0] ?>');">
							<div class="contents">
								<h2><?php the_title(); ?><?php if ( get_field('degrees') ) { ?>, <?php the_field('degrees'); ?><?php } ?></h2>
								<p><?php the_field('job_title'); ?></p>
								<?php if ( in_array('Warren', get_field('location')) && in_array('Dearborn', get_field('location')) && in_array('Livonia', get_field('location')) && in_array('Roseville', get_field('location')) && in_array('Auburn Hills', get_field('location')) ) { //ALL LOCATIONS?>
									<p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg>All Locations</p>
								<?php } else { ?>
									<p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg><?php the_field('location'); ?></p>
								<?php } ?>
							</div>
							<div class="overlay overlay--black-fade"></div>
						</div>
						<div class="bio">
							<?php if ( has_excerpt() ) {
								the_excerpt(); 
							} else {
								$bio = get_field('bio');
								$summary = substr($bio, 0, 150);
								echo $summary;
							} ?>
							<div class="preview-link">Read the Rest
								<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path class="st0" fill="#24CE5F" d="M4.8 24.2c-.4 0-.8-.1-1.2-.3-.8-.4-1.2-1.2-1.2-2.1v-18c0-.9.5-1.7 1.2-2.1.4-.3.8-.4 1.2-.4.4 0 .8.1 1.2.3l15.8 8.8c.8.4 1.2 1.2 1.2 2.1 0 .9-.5 1.7-1.2 2.1l-15.7 9.3c-.4.2-.8.3-1.3.3z"></path></svg>
							</div>
						</div>
					</a>
				<?php } ?>
      </div>
		</div>
		<?php } ?>
		<?php wp_reset_query(); ?>
		<?php 
			$location = get_the_title();
			$args = array( 
				'posts_per_page'  => -1, 
				'post_type' => 'staff',
				'orderby' => 'date',
				'order' => 'ASC',
				'category_name' => 'orthotists',
				'meta_query'  => array(
					'relation'    => 'OR',
					array(
						'key'   => 'location',
						'value'   => $location,
						'compare' => 'LIKE'
					),
				)
			);
			$query = new WP_Query( $args );
		?>
		<?php if ( $query->have_posts() ) { ?>
    <div class="block block--flex block--full">
      <a id="orthotists" class="anchor"></a>
      <div class="block block--half">
        <div class="staff-title block block--full block--dark">
          <h1>Orthotists</h1>
        </div>
      </div>
      <div class="block block--half">
				<?php while ( $query->have_posts() ) { $query->the_post(); ?>
					<?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium'); ?>
					<a href="<?php the_permalink(); ?>" class="preview-staff block block--full">
						<div class="intro block block--dark" style="background-image: url('<?php echo $featured[0] ?>');">
							<div class="contents">
								<h2><?php the_title(); ?><?php if ( get_field('degrees') ) { ?>, <?php the_field('degrees'); ?><?php } ?></h2>
								<p><?php the_field('job_title'); ?></p>
								<?php if ( get_field('location') ) { ?>
									<p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg><?php the_field('location'); ?></p>
								<?php } ?>
							</div>
							<div class="overlay overlay--black-fade"></div>
						</div>
						<div class="bio">
							<?php if ( has_excerpt() ) {
								the_excerpt(); 
							} else {
								$bio = get_field('bio');
								$summary = substr($bio, 0, 150);
								echo $summary;
							} ?>
							<div class="preview-link">Read the Rest
								<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path class="st0" fill="#24CE5F" d="M4.8 24.2c-.4 0-.8-.1-1.2-.3-.8-.4-1.2-1.2-1.2-2.1v-18c0-.9.5-1.7 1.2-2.1.4-.3.8-.4 1.2-.4.4 0 .8.1 1.2.3l15.8 8.8c.8.4 1.2 1.2 1.2 2.1 0 .9-.5 1.7-1.2 2.1l-15.7 9.3c-.4.2-.8.3-1.3.3z"></path></svg>
							</div>
						</div>
					</a>
				<?php } ?>
      </div>
    </div>
		<?php } ?>
  </div>
	<?php wp_reset_query(); ?>
	<?php 
		$location = get_the_title();
		$args = array( 
			'posts_per_page'  => -1, 
			'orderby' => 'date',
			'order' => 'ASC',
			'post_type' => 'staff',
			'category_name' => 'staff',
			'meta_query'  => array(
				'relation'    => 'OR',
				array(
					'key'   => 'location',
					'value'   => $location,
					'compare' => 'LIKE'
				),
			)
		);
		$query = new WP_Query( $args );
	?>
	<?php if ( $query->have_posts() ) { ?>
  <div class="basic-staff block block--full block--dark">
    <a id="staff" class="anchor"></a>
    <h1>Staff</h1>
    <div class="block block--flex block--max">
			<?php while ( $query->have_posts() ) { $query->the_post(); ?>
				<?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium'); ?>
				<div class="preview-staff block block--third-full">
					<div class="intro block block--dark" style="background-image: url('<?php echo $featured[0] ?>');">
						<div class="contents">
							<h2><?php the_title(); ?><?php if ( get_field('degrees') ) { ?>, <?php the_field('degrees'); ?><?php } ?></h2>
							<p><?php the_field('job_title'); ?></p>
							<?php if ( get_field('location') ) { ?>
								<p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg><?php the_field('location'); ?></p>
							<?php } ?>
						</div>
						<div class="overlay overlay--black-fade"></div>
					</div>
				</div>
			<?php } ?>
    </div>
  </div>
	<?php } ?>
</div>

	<div class="closing-container block block--full" style="background-image: url('/wp-content/uploads/2016/05/water-edited.jpg');">
		<div class="block block--flex block--max">
			<div class="closing-question block block--dark">
				<img src="/wp-content/uploads/2018/09/pin-tri.svg" alt="location pointer" />
				<h2>Make an Appointment</h2>
				<p>Do you want to be seen by one of our physical therapists? Make an appointment online. </p>
				<a href="/make-an-appointment/" class="btn btn--secondary" target="">Schedule Appointment</a>
			</div>
		</div>
	</div>
  
</div>

<?php get_footer(); ?>