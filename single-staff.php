<?php
/****************************************************************************
  SINGLE STAFF PAGE
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <div class="block block--max block--flex">
    <nav class="nav--secondary block block--half">
      <ul>
        <li><a href="/our-staff/#therapists" class="smoothScroll">Therapists</a></li>
        <li><a href="/our-staff/#orthotists" class="smoothScroll">Orthotists</a></li>
        <li><a href="/our-staff/#staff" class="smoothScroll">Staff</a></li>
      </ul>
    </nav>
    <div class="block block--half"></div>
  </div>

  <div class="intro-container block block--max block--flex">
    <div class="intro-title-container block block--half-full block--dark">
      <h1><?php the_title(); ?><?php if ( get_field('degrees') ) { ?>, <?php the_field('degrees'); ?><?php } ?></h1>
      <p class="job_title"><?php the_field('job_title'); ?>
        <?php if ( in_array('Warren', get_field('location')) && in_array('Dearborn', get_field('location')) && in_array('Livonia', get_field('location')) && in_array('Roseville', get_field('location')) && in_array('Auburn Hills', get_field('location')) ) { //ALL LOCATIONS?>
          <span><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg>ALL LOCATIONS</span>
        <?php } else { ?>
          <span><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg><?php the_field('location'); ?></span>
        <?php } ?>
      </p>
      <?php the_field('bio'); ?>
    </div>
    <?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium'); ?>
    <div class="intro-contents-container block block--half-full" style="background-image: url('<?php echo $featured[0] ?>');">
      <div class="overlay overlay--gradient"></div>
    </div>
  </div>

  <?php get_template_part( 'template-parts/content', 'logo-slider' ); ?>
  
</div>

<?php get_footer(); ?>