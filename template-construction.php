<?php 
/****************************************************************************
  Template Name: Construction
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php $background = get_field('background_image'); ?>
  <section class="title-container title-container--full block block--full" style="background-image: url('<?php echo $background['url']; ?>');">
    <div class="block block--flex">
      <div class="block block--max">
        <?php if ( get_field('main_heading') ) { ?>
          <p class="pre-heading"><?php the_field('pre-heading'); ?></p>
          <p class="main-heading"><?php the_field('main_heading'); ?></p>
          <p class="post-heading" style="max-width: 400px; margin-bottom: -1rem;"><?php the_field('post-heading'); ?></p>
        <?php } else { ?>
          <p class="main-heading"><?php the_title(); ?></p>
        <?php } ?>
        <?php if (  is_page_template( 'template-construction.php' ) ) { echo do_shortcode('[gravityform id="4" title="false" description="false"]'); } ?>
      </div>
    </div>
    <div class="overlay overlay--gradient"></div>
  </section>

</div>

<?php get_footer(); ?>