<?php 
/****************************************************************************
  Template Name: Staff
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <div class="block block--max block--flex">
    <nav class="nav--secondary block block--half">
      <ul>
        <li><a href="#therapists" class="smoothScroll">Therapists</a></li>
        <!-- <li><a href="#orthotists" class="smoothScroll">Orthotist/Prosthetists</a></li> -->
        <li><a href="#staff" class="smoothScroll">Staff</a></li>
      </ul>
    </nav>
		<?php 
			$args = array(
				'post_type' => array('location'),
				'posts_per_page' => -1,
				'orderby' => 'title',
				'order' => 'ASC',
			);
			$location = new WP_Query( $args );
		?>
		<?php if ( $location->have_posts() ) { ?>

		<div class="location-filter block block--half">
			<form method="post">
				<label>Filter by location</label>
				<select name='PreviousReceiver' onchange="window.location='index.php?city='+this.value">
					<option value=''>Select...</option>

					<?php while ( $location->have_posts() ) { $location->the_post(); ?>

						<option value='<?php the_title(); ?>'><?php the_title(); ?></option>

					<?php } ?>

				</select>
			</form>
		</div>

		<?php } ?>
		<?php wp_reset_postdata(); ?>
  </div>

  <div class="special-staff block block--max default-contents">
    <div class="block block--flex block--full">
      <a id="therapists" class="anchor"></a>
      <div class="block block--half">
        <div class="staff-title block block--full block--dark">
          <h1>Therapists</h1>
        </div>
      </div>
      <div class="block block--half">
        <?php 
          if ( isset($_GET['city']) ) {
            $city = $_GET['city'];
            $args = array( 
              'posts_per_page'  => -1, 
              'orderby' => 'date',
              'order' => 'ASC',
              'post_type' => 'staff',
              'category_name' => 'therapists',
              'meta_query'  => array(
                'relation'    => 'OR',
                array(
                  'key'   => 'location',
                  'value'   => $city,
                  'compare' => 'LIKE'
                ),
              )
            );
          } else {
            $args = array( 
              'posts_per_page'  => -1, 
              'orderby' => 'date',
              'order' => 'ASC',
              'post_type' => 'staff',
              'category_name' => 'therapists',
            );
          }
          $query = new WP_Query( $args );
        ?>
        <?php if ( $query->have_posts() ) { $i = 0; ?>
          <?php while ( $query->have_posts() ) { $query->the_post(); $i++; ?>
            <?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium'); ?>
            <a href="<?php the_permalink(); ?>" class="preview-staff block block--full <?php if( $i == 1) { ?>first<?php } ?>">
              <div class="intro block block--dark" style="background-image: url('<?php echo $featured[0] ?>');">
                <div class="contents">
                  <h2><?php the_title(); ?><?php if ( get_field('degrees') ) { ?>, <?php the_field('degrees'); ?><?php } ?></h2>
                  <p><?php the_field('job_title'); ?></p>
                  <?php if ( in_array('Warren', get_field('location')) && in_array('Dearborn', get_field('location')) && in_array('Livonia', get_field('location')) && in_array('Roseville', get_field('location')) && in_array('Auburn Hills', get_field('location')) ) { //ALL LOCATIONS?>
                    <p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg>All Locations</p>
                  <?php } else { ?>
                    <p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg><?php the_field('location'); ?></p>
                  <?php } ?>
                </div>
                <div class="overlay overlay--black-fade"></div>
              </div>
              <div class="bio">
                <?php if ( has_excerpt() ) {
                  the_excerpt(); 
                } else {
                  $bio = get_field('bio');
                  $summary = substr($bio, 0, 150);
                  echo $summary;
                } ?>
                <div class="preview-link">Read the Rest
                  <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path class="st0" fill="#24CE5F" d="M4.8 24.2c-.4 0-.8-.1-1.2-.3-.8-.4-1.2-1.2-1.2-2.1v-18c0-.9.5-1.7 1.2-2.1.4-.3.8-.4 1.2-.4.4 0 .8.1 1.2.3l15.8 8.8c.8.4 1.2 1.2 1.2 2.1 0 .9-.5 1.7-1.2 2.1l-15.7 9.3c-.4.2-.8.3-1.3.3z"></path></svg>
                </div>
              </div>
            </a>
          <?php } ?>
        <?php } else { ?>
          <h2>No Therapists at this location</h2>
        <?php } ?>
      </div>
    </div>
    <!-- <div class="block block--flex block--full">
      <a id="orthotists" class="anchor"></a>
      <div class="block block--half">
        <div class="staff-title block block--full block--dark">
          <h1>Orthotist/Prosthetists</h1>
        </div>
      </div>
      <div class="block block--half">
        <?php 
          if ( isset($_GET['city']) ) {
            $city = $_GET['city'];
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'staff',
              'orderby' => 'date',
              'order' => 'ASC',
              'category_name' => 'orthotists',
              'meta_query'  => array(
                'relation'    => 'OR',
                array(
                  'key'   => 'location',
                  'value'   => $city,
                  'compare' => 'LIKE'
                ),
              )
            );
          } else {
            $args = array( 
              'posts_per_page'  => -1,
              'orderby' => 'date',
              'order' => 'ASC',
              'post_type' => 'staff',
              'category_name' => 'orthotists',
            );
          }
          $query = new WP_Query( $args );
        ?>
        <?php if ( $query->have_posts() ) { ?>
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium'); ?>
            <a href="<?php the_permalink(); ?>" class="preview-staff block block--full">
              <div class="intro block block--dark" style="background-image: url('<?php echo $featured[0] ?>');">
                <div class="contents">
                  <h2><?php the_title(); ?><?php if ( get_field('degrees') ) { ?>, <?php the_field('degrees'); ?><?php } ?></h2>
                  <p><?php the_field('job_title'); ?></p>
                  <?php if ( get_field('location') ) { ?>
                    <p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg><?php the_field('location'); ?></p>
                  <?php } ?>
                </div>
                <div class="overlay overlay--black-fade"></div>
              </div>
              <div class="bio">
                <?php if ( has_excerpt() ) {
                  the_excerpt(); 
                } else {
                  $bio = get_field('bio');
                  $summary = substr($bio, 0, 150);
                  echo $summary;
                } ?>
                <div class="preview-link">Read the Rest
                  <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path class="st0" fill="#24CE5F" d="M4.8 24.2c-.4 0-.8-.1-1.2-.3-.8-.4-1.2-1.2-1.2-2.1v-18c0-.9.5-1.7 1.2-2.1.4-.3.8-.4 1.2-.4.4 0 .8.1 1.2.3l15.8 8.8c.8.4 1.2 1.2 1.2 2.1 0 .9-.5 1.7-1.2 2.1l-15.7 9.3c-.4.2-.8.3-1.3.3z"></path></svg>
                </div>
              </div>
            </a>
          <?php } ?>
        <?php } else { ?>
          <h2>No Orthotist/Prosthetists at this location</h2>
        <?php } ?>
      </div>
    </div> -->
  </div>

  <div class="basic-staff block block--full block--dark">
    <a id="staff" class="anchor"></a>
    <h1>Staff</h1>
    <div class="block block--flex block--max">
      <?php 
        if ( isset($_GET['city']) ) {
          $city = $_GET['city'];
          $args = array( 
            'posts_per_page'  => -1, 
            'orderby' => 'date',
            'order' => 'ASC',
            'post_type' => 'staff',
            'category_name' => 'staff',
            'meta_query'  => array(
                'relation'    => 'OR',
                array(
                  'key'   => 'location',
                  'value'   => $city,
                  'compare' => 'LIKE'
                ),
              )
          );
        } else {
          $args = array( 
            'posts_per_page'  => -1, 
            'orderby' => 'date',
            'order' => 'ASC',
            'post_type' => 'staff',
            'category_name' => 'staff',
          );
        }
        $query = new WP_Query( $args );
      ?>
      <?php if ( $query->have_posts() ) { ?>
        <?php while ( $query->have_posts() ) { $query->the_post(); ?>
          <?php $featured = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'medium'); ?>
          <div class="preview-staff block block--third-full">
            <div class="intro block block--dark" style="background-image: url('<?php echo $featured[0] ?>');">
              <div class="contents">
                <h2><?php the_title(); ?><?php if ( get_field('degrees') ) { ?>, <?php the_field('degrees'); ?><?php } ?></h2>
                <p><?php the_field('job_title'); ?></p>
                <?php if ( get_field('location') ) { ?>
                  <p class="location"><svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 25 25" enable-background="new 0 0 25 25"><path fill="#24CE5F" d="M6.5 9.3c0 3.3 2.7 5.9 5.9 6 3.3 0 5.9-2.7 6-5.9 0-3.3-2.7-5.9-5.9-6s-6 2.6-6 5.9zm-3 0c.2-4.9 4.4-8.7 9.3-8.5 4.6.2 8.3 3.9 8.5 8.5 0 4.9-8.9 14.9-8.9 14.9s-8.9-9.9-8.9-14.9z"></path></svg><?php the_field('location'); ?></p>
                <?php } ?>
              </div>
              <div class="overlay overlay--black-fade"></div>
            </div>
          </div>
        <?php } ?>
      <?php } else { ?>
        <h2>No Staff Members at this location</h2>
      <?php } ?>
    </div>
  </div>

  <?php if ( get_field('display_logo_slider') == 'yes' ) {
    get_template_part( 'template-parts/content', 'logo-slider' ); 
  } ?>

  <?php if ( get_field('display_question') == 'yes' ) {
    get_template_part( 'template-parts/content', 'closing-question' ); 
  } ?>
  
</div>

<?php get_footer(); ?>