<?php
/****************************************************************************
  THE TEMPLATE FOR DISPLAYING SINGLE BLOG POST
****************************************************************************/
?>

<?php get_header(); ?>

<div class="block page-container">

  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <?php if ( get_field('intro_title') ) { 
    get_template_part( 'template-parts/content', 'page-intro' ); 
  } ?>

  <?php if ( $post->post_content != "" ) { ?>
    <div class="block block--max article-contain">
			<article><?php the_content(); ?></article>
			<aside><?php dynamic_sidebar( 'single_post' ); ?></aside>
    </div>
  <?php } ?>
  
  <?php if ( have_rows('cards') ) {
    get_template_part( 'template-parts/content', 'cards' ); 
  } ?>

  <?php if ( get_field('display_logo_slider') == 'yes' ) {
    get_template_part( 'template-parts/content', 'logo-slider' ); 
  } ?>

  <?php if ( get_field('display_question') == 'yes' ) {
    get_template_part( 'template-parts/content', 'closing-question' ); 
  } ?>
  
</div>

<?php get_footer(); ?>
