<?php

/*-----------------------------------------
  INCLUDE CSS AND JS
-----------------------------------------*/
function wp_main_assets() {
	wp_register_style( 'google_fonts', 'https://fonts.googleapis.com/css?family=Lato:300,300i,700,900|Poppins:600,700', array(), '1.1', 'all' );
  wp_enqueue_style('google_fonts');
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
	wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array (), 1.1, true);
	wp_enqueue_script('maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBxlx86WdZSESQTjGusB9YEM3p5rZSAgPU', array (), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array ( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*-----------------------------------------
  THEME STYLES IN THE VISUAL EDITOR
-----------------------------------------*/
add_editor_style('/assets/styles/main.css');

/*-----------------------------------------
  Enable Excerpts
-----------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
add_post_type_support( 'post', 'excerpt' );

/*-----------------------------------------
  Enable Post Thumbnails
-----------------------------------------*/
add_theme_support('post-thumbnails');

/*-----------------------------------------
  Enable HTML5 Markup Support
-----------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/****************************************************************************
  STYLEGUIDE HEX TO RGB
****************************************************************************/
function hex2rgb( $colour ) {
  if ( $colour[0] == '#' ) { 
    $colour = substr( $colour, 1 ); 
  }
  if ( strlen( $colour ) == 6 ) {
    list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
  } elseif ( strlen( $colour ) == 3 ) {
    list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
  } else {
    return false;
  }
  $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
  return 'rgb('.$r.', '.$g.', '.$b.')';
}
/****************************************************************************
  STYLEGUIDE RGB TO CMYK
****************************************************************************/
function hex2rgb2($hex) {
  $color = str_replace('#','',$hex);
  $rgb = array(
    'r' => hexdec(substr($color,0,2)),
    'g' => hexdec(substr($color,2,2)),
    'b' => hexdec(substr($color,4,2)),
  );
  return $rgb;
}
function rgb2cmyk($var1,$g=0,$b=0) {
  if (is_array($var1)) { 
    $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
  } else { 
    $r = $var1; 
  }
  $r = $r / 255; $g = $g / 255; $b = $b / 255;
  $bl = 1 - max(array($r,$g,$b));
  if ( ( 1 - $bl ) > 0 ) {
    $c = ( 1 - $r - $bl ) / ( 1 - $bl ); 
    $m = ( 1 - $g - $bl ) / ( 1 - $bl ); 
    $y = ( 1 - $b - $bl ) / ( 1 - $bl );
  }
  $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
  return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
}
/****************************************************************************
  REMOVE BLOG AS CPT PARENT
****************************************************************************/
function wpdev_nav_classes( $classes, $item ) {
	if( ( is_post_type_archive( 'staff' ) || is_singular( 'staff' ) || is_page(447) )
			&& $item->title == 'Blog' ){
			$classes = array_diff( $classes, array( 'current_page_parent' ) );
	}
	return $classes;
}
add_filter( 'nav_menu_css_class', 'wpdev_nav_classes', 10, 2 );