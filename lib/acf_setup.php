<?php

/****************************************************************************
  ENABLE ACF OPTIONS PAGE
****************************************************************************/
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
  acf_add_options_sub_page('Theme Options');
  acf_add_options_sub_page('Default Images');
}

/*-----------------------------------------
  GOOGLE MAP
-----------------------------------------*/
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyBxlx86WdZSESQTjGusB9YEM3p5rZSAgPU');
}
add_action('acf/init', 'my_acf_init');

/****************************************************************************
  ADD ACF TO BE USED IN THE RELEVANSSI CUSTOM EXCERPTS
****************************************************************************/
function custom_fields_to_excerpts($content, $post, $query) {
  $custom_fields = get_post_custom_keys($post->ID);
  $remove_underscore_fields = true;
  if (is_array($custom_fields)) {
    $custom_fields = array_unique($custom_fields);  // no reason to index duplicates
    foreach ($custom_fields as $field) {
      if ($remove_underscore_fields) {
        if (substr($field, 0, 1) == '_') continue;
      }
      $values = get_post_meta($post->ID, $field, false);
      if ("" == $values) continue;
      foreach ($values as $value) {
        if ( !is_array ( $value ) ) {
          $content .= " | " . $value;
        }
      }
    }
  }
  return $content;
}
add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
?>