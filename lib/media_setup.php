<?php

/*-----------------------------------------
  ADDITIONAL IMAGE SIZE
-----------------------------------------*/
add_image_size( 'small', 400, 400 ); 
function display_image_sizes($sizes) {
	$sizes['small'] = __( 'Small' );
	return $sizes;
}
add_filter('image_size_names_choose', 'display_image_sizes');

/*-----------------------------------------
  ALLOW SVG & ZIP FILES IN WORDPRESS
-----------------------------------------*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['zip'] = 'application/zip';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
function bodhi_svgs_disable_real_mime_check( $data, $file, $filename, $mimes ) {
  $wp_filetype = wp_check_filetype( $filename, $mimes );
  $ext = $wp_filetype['ext'];
  $type = $wp_filetype['type'];
  $proper_filename = $data['proper_filename'];
  return compact( 'ext', 'type', 'proper_filename' );
}
add_filter( 'wp_check_filetype_and_ext', 'bodhi_svgs_disable_real_mime_check', 10, 4 );

/*-----------------------------------------
  ENABLE TAGS FOR MEDIA ITEMS
-----------------------------------------*/
function wptp_add_tags_to_attachments() {
	register_taxonomy_for_object_type( 'post_tag', 'attachment' );
}
add_action( 'init' , 'wptp_add_tags_to_attachments' );