<?php

/*-----------------------------------------
  WHITELABEL ADMIN AREA
-----------------------------------------*/
function my_custom_admin() {
  echo '
    <style>
      a:hover, a:active { color: #e48849; }
      #wpcontent { 
        max-width:100%; 
        min-height: calc(100vh - 110px); 
      }
      .wp-core-ui .button:hover {
        background-color: #e5e5e5 !important;
      }
      .wp-core-ui .button-primary, .wrap .add-new-h2, .wrap .add-new-h2:active, 
      .wrap .page-title-action, .wrap .page-title-action:active {     
        background: #e48849 !important;
        border-color: #e48849 !important;
        text-shadow: none !important;
        box-shadow: none !important;
        color: #fff !important;
      }
      .wp-core-ui .button-primary:hover, .wrap .add-new-h2:hover,
      .wrap .page-title-action:hover {
        background-color: #ce7b42 !important;
        border-color: #ce7b42 !important;
        color: #fff !important;
      }
      .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover,
      .wp-core-ui .button:focus, .wp-core-ui .button:hover {
        box-shadow: none;
      }
      .flatty-top-bar-button, .flatty-top-bar a:hover, .flatty-top-bar-button, 
      .flatty-top-bar a:active { 
        color: #fff; 
      }
    </style>
  ';
}
add_action('admin_head', 'my_custom_admin');