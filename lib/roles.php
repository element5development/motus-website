<?php

/*-----------------------------------------
		Edit User Roles
-----------------------------------------*/
function add_grav_forms(){
  $role = get_role('editor');
  $role->add_cap('gform_full_access');
  $role->add_cap('edit_theme_options');
  $role->add_cap('edit_users');
  $role->add_cap('list_users');
  $role->add_cap('promote_users');
  $role->add_cap('create_users');
  $role->add_cap('add_users');
  $role->add_cap('delete_users');
}
add_action('admin_init','add_grav_forms');