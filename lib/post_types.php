<?php

/*-----------------------------------------
	CUSTOM POST TYPES - www.wp-hasty.com
-----------------------------------------*/
/****************************************************************************
  STAFF MEMBERS CUSTOM POST TYPE
****************************************************************************/
function create_staff_cpt() {
  $labels = array(
    'name' => __( 'Staff', 'Post Type General Name', 'textdomain' ),
    'singular_name' => __( 'Staff', 'Post Type Singular Name', 'textdomain' ),
    'menu_name' => __( 'Staff', 'textdomain' ),
    'name_admin_bar' => __( 'Staff', 'textdomain' ),
    'archives' => __( 'Staff Archives', 'textdomain' ),
    'attributes' => __( 'Staff Attributes', 'textdomain' ),
    'parent_item_colon' => __( 'Parent Staff:', 'textdomain' ),
    'all_items' => __( 'All Staff', 'textdomain' ),
    'add_new_item' => __( 'Add New Staff', 'textdomain' ),
    'add_new' => __( 'Add New', 'textdomain' ),
    'new_item' => __( 'New Staff', 'textdomain' ),
    'edit_item' => __( 'Edit Staff', 'textdomain' ),
    'update_item' => __( 'Update Staff', 'textdomain' ),
    'view_item' => __( 'View Staff', 'textdomain' ),
    'view_items' => __( 'View Staff', 'textdomain' ),
    'search_items' => __( 'Search Staff', 'textdomain' ),
    'not_found' => __( 'Not found', 'textdomain' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
    'featured_image' => __( 'Featured Image', 'textdomain' ),
    'set_featured_image' => __( 'Set featured image', 'textdomain' ),
    'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
    'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
    'insert_into_item' => __( 'Insert into Staff', 'textdomain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this Staff', 'textdomain' ),
    'items_list' => __( 'Staff list', 'textdomain' ),
    'items_list_navigation' => __( 'Staff list navigation', 'textdomain' ),
    'filter_items_list' => __( 'Filter Staff list', 'textdomain' ),
  );
  $args = array(
    'label' => __( 'Staff', 'textdomain' ),
    'description' => __( 'Staff Members of Motus Rehabilitation', 'textdomain' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-businessman',
    'supports' => array('title', 'thumbnail', 'excerpt' ),
    'taxonomies' => array('category', ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_admin_bar' => false,
    'show_in_nav_menus' => false,
    'can_export' => false,
    'has_archive' => false,
    'hierarchical' => false,
    'exclude_from_search' => true,
    'show_in_rest' => true,
    'publicly_queryable' => true,
    'capability_type' => 'post',
  );
  register_post_type( 'staff', $args );
}
add_action( 'init', 'create_staff_cpt', 0 );

// Post Type Key: location
function create_location_cpt() {

	$labels = array(
		'name' => __( 'Locations', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Location', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Locations', 'textdomain' ),
		'name_admin_bar' => __( 'Location', 'textdomain' ),
		'archives' => __( 'Location Archives', 'textdomain' ),
		'attributes' => __( 'Location Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Location:', 'textdomain' ),
		'all_items' => __( 'All Locations', 'textdomain' ),
		'add_new_item' => __( 'Add New Location', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Location', 'textdomain' ),
		'edit_item' => __( 'Edit Location', 'textdomain' ),
		'update_item' => __( 'Update Location', 'textdomain' ),
		'view_item' => __( 'View Location', 'textdomain' ),
		'view_items' => __( 'View Locations', 'textdomain' ),
		'search_items' => __( 'Search Location', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Location', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Location', 'textdomain' ),
		'items_list' => __( 'Locations list', 'textdomain' ),
		'items_list_navigation' => __( 'Locations list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Locations list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Location', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-location-alt',
		'supports' => array('title', 'editor', 'post-formats', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'location', $args );

}
add_action( 'init', 'create_location_cpt', 0 );