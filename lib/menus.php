<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function register_menu () {
  register_nav_menu('primary-nav-right', __('Primary Navigation Right'));
  register_nav_menu('primary-nav-left', __('Primary Navigation Left'));
	register_nav_menu('footer-nav', __('Footer Navigation'));
	register_nav_menu('location-nav', __('Location Navigation'));
}
add_action('init', 'register_menu');

/*-----------------------------------------
  SEO Yoast Breadcrumbs
-----------------------------------------*/
add_theme_support( 'yoast-seo-breadcrumbs' );
function jb_crumble_bread($link_text, $id) {
	$link_text = html_entity_decode($link_text);
	$crumb_length = strlen( $link_text );
 	$crumb_size = 14;
 	$crumble = substr( $link_text, 0, $crumb_size );
	if ( $crumb_length > $crumb_size ) {
		$crumble .= '...';
	}
	return $crumble;
}
add_filter('wp_seo_get_bc_title', 'jb_crumble_bread', 10, 2);