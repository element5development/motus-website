<?php

/*-----------------------------------------
		SIDEBARS - www.wp-hasty.com
-----------------------------------------*/
function custom_sidebars() {
	$args = array(
		'name'          => __( 'single_post', 'textdomain' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'custom_sidebars' );