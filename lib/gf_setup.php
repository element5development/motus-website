<?php

/*-----------------------------------------
  CORRECT TAB INDEX ON FORMS
-----------------------------------------*/
add_filter("gform_tabindex", create_function("", "return false;"));

/*-----------------------------------------
  EABLE LABEL VISIBILITY OPTION
-----------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*-----------------------------------------
  SUBMIT INPUT TO BUTTON ELEMENT
-----------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="btn btn--secondary"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );

/****************************************************************************
  CHANGE GRAVITY FORM EMAILS TO TEXT INSTEAD OF HTML
****************************************************************************/
add_filter( 'gform_notification', 'change_notification_format', 10, 3 );
function change_notification_format( $notification, $form, $entry ) {
    $notification['message_format'] = 'text';
    return $notification;
}