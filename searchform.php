<?php
/* Template for displaying search form*/
?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" id="<?php echo $unique_id; ?>" class="search-field" placeholder="searching for..." value="<?php echo get_search_query(); ?>" name="s" />
  <input type="submit" class="search-submit btn btn--primary" value="search" />
</form>
